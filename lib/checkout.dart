import 'package:flutter/material.dart';
import 'package:foomart/Components/BlockButton.dart';

import 'Components/CustomListTile.dart';
import 'Components/SearchAndLocationAppBar.dart';
import 'Constants.dart';

class Checkout extends StatefulWidget {
  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return SafeArea(
        child: Scaffold(
      backgroundColor: Color(0xFFF9F9F9),
      body: Container(
        height: _size.height,
        width: _size.width,
        margin: EdgeInsets.symmetric(vertical: 10),
        child: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:8.0),
                  child: SearchAndLocationAppBar(),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:15.0),
                  child: Container(
                    margin: EdgeInsets.only(bottom: 7.0),
                    width: _size.width,
                    constraints: BoxConstraints(maxHeight: 145.0, minHeight: 120.0),
                    child: Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: Placeholder(
                            color: Colors.blueGrey,
                          ),
                        ),
                        Flexible(
                          flex: 2,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Imperial Restaurant".toUpperCase(),
                                  style: AppConstants.bodyFont.apply(
                                      fontWeightDelta: 2,
                                      color: Color(0xff3E3F68)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Icon(Icons.star, color: AppConstants.appGreen),
                                    Text(
                                      " 4.9 ",
                                      style: AppConstants.bodyFont
                                          .apply(fontSizeFactor: 0.7, fontWeightDelta: 2, color: AppConstants.appGreen),
                                    ),
                                    Text(
                                      " (144 ratings)",
                                      style: AppConstants.bodyFont
                                          .apply(fontSizeFactor: 0.7),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "Indian Arabic",
                                  style: AppConstants.bodyFont
                                      .apply(fontSizeFactor: 0.7),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Flexible(flex:1,child: Icon(Icons.location_pin, color: AppConstants.appGreen,)),
                                    SizedBox(width: 5.0,),
                                    Flexible(
                                      flex: 6,
                                      child: Text(
                                        "No. 30, Millers Road No. 30, Millers Road No. 30, Millers Road No. 30, Millers Roa",
                                        style: AppConstants.bodyFont
                                            .apply(fontSizeFactor: 0.7),
                                      ),
                                    ),
                                  ],
                                )

                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                ListTile(
                  tileColor: AppConstants.appGreen.withOpacity(0.35),
                  title: Text("Tandoori Half Chicken X1 Half", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7),),
                  trailing: Text("\u20B9 190", style: AppConstants.bodyFont.apply(fontWeightDelta: 3, color: Colors.black),),
                ),
                ListTile(
                  tileColor: AppConstants.appGreen.withOpacity(0.35),
                  title: Text("Tandoori Half Chicken X1 Half", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7),),
                  trailing: Text("\u20B9 190", style: AppConstants.bodyFont.apply(fontWeightDelta: 3, color: Colors.black),),
                ),
                ListTile(
                  tileColor: AppConstants.appGreen.withOpacity(0.35) ,
                  title: Text("Tandoori Half Chicken X1 Half", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7),),
                  trailing: Text("\u20B9 190", style: AppConstants.bodyFont.apply(fontWeightDelta: 3, color: Colors.black),),
                ),
                ListTile(
                  tileColor: AppConstants.appGreen.withOpacity(0.35),
                  title: Text("Tandoori Half Chicken X1 Half", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7),),
                  trailing: Text("\u20B9 190", style: AppConstants.bodyFont.apply(fontWeightDelta: 3, color: Colors.black),),
                ),
                ListTile(
                  title: Text("Delivery Instructions",  style: AppConstants.headingFont.apply(color: Colors.black), ),
                  trailing: TextButton(style:TextButton.styleFrom(padding: EdgeInsets.only(right:0)),onPressed:(){},child: Text("+ Add Notes"),),
                ),
                CustomListTile(
                  title: Text("Sub Total", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, fontWeightDelta: 3, color: Colors.black)),
                  trailing: Text("\u20B9 68", style: AppConstants.headingFont,),
                ),
                CustomListTile(
                  title: Text("Delivery Cost", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, fontWeightDelta: 3, color: Colors.black)),
                  trailing: Text("\u20B9 2", style: AppConstants.headingFont,),
                ),
                CustomListTile(
                  title: Text("Total", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, fontWeightDelta: 3, color: Colors.black)),
                  trailing: Text("\u20B9 70", style: AppConstants.headingFont,),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:15.0),
                  child: BlockButton(btnLabel: "Checkout"),
                ),

              ]),
        ),
      ),
      bottomNavigationBar: Container(
            color: Color(0xFFf6f6f6),
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            height: 60,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: _size.width/2.5 - 16*2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.home_filled),
                        ),
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.bookmark),
                        ),
                      ],
                    ),
                  ),
                ),

                Positioned(
                  top: -20,
                  left: _size.width/2 - 45,
                  child: Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(1000),color: AppConstants.appGreen,),
                    child: GestureDetector(
                      onTap: ()=>{},
                      child: Icon(Icons.shopping_cart_rounded, color: Colors.white,),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: _size.width/2.5 - 16*2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.notifications),
                        ),
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.account_circle_rounded),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
    ));
  }
}
