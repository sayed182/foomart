import 'package:flutter/material.dart';
import 'package:foomart/Components/BlockButton.dart';
import 'package:foomart/Constants.dart';
import 'package:google_fonts/google_fonts.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  FocusNode _nameTextFocusNode = new FocusNode();
  FocusNode _emailTextFocusNode = new FocusNode();

  TextEditingController _nameTextController = new TextEditingController();
  TextEditingController _emailTextController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;

    return SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
        body: SingleChildScrollView(child: Container(height: _size.height, width: _size.width, margin: EdgeInsets.symmetric(horizontal: 52*_ratio),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 130 * _ratio),
              Image.asset("assets/logo.png", height: 195),
              SizedBox(height: 80 * _ratio,),
              Text("Let's Discover\nThousands of restaurants", style: AppConstants.bodyFont ,textAlign: TextAlign.center,),
              SizedBox(height: 75 * _ratio,),
              TextFormField(
                controller: _nameTextController,
                focusNode: _nameTextFocusNode,
                decoration: InputDecoration(
                  hintText: "Your Name",
                  hintStyle: AppConstants.bodyFont,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide.none
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide.none
                  ),
                  fillColor: AppConstants.appGreen.withOpacity(0.4),
                  filled: true,
                ),
              ),
              SizedBox(height: 20,),
              TextFormField(
                controller: _emailTextController,
                focusNode: _emailTextFocusNode,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  hintText: "Your Email",
                  hintStyle: AppConstants.bodyFont,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide.none
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide.none
                  ),
                  fillColor: AppConstants.appGreen.withOpacity(0.4),
                  filled: true,
                ),
              ),
              SizedBox(height: 70 *_ratio,),
              BlockButton(btnLabel: "NEXT", btnFunction: (){},)
            ],
          ),
        )),
    ));
  }
}
