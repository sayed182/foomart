import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppConstants {
  static const double appReponsiveRatio = 0.47;
  static const Color appGreen = Color(0xFF00D589);
  static const Color appGrey = Color(0xFFC2C2C2);
  static const Color textGrey = Color(0xff7C7D7E);
  static TextStyle bodyFont = GoogleFonts.josefinSans(
    fontSize: 20,
    color: Color(
      0xff7C7D7E,
    ),
    fontWeight: FontWeight.w600,
  );

  static TextStyle headingFont = GoogleFonts.josefinSans(
    fontSize: 16,
    color: AppConstants.appGreen,
    fontWeight: FontWeight.bold,
  ).apply(fontWeightDelta: 4);

  static LinearGradient orangeGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFFFF8C48).withOpacity(0.75),
      Color(0xFFFF56AB).withOpacity(0.75)
    ],
  );

  static LinearGradient greenGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFF1D976C).withOpacity(0.75),
      Color(0xFF93F9B9).withOpacity(0.75)
    ],
  );

  static LinearGradient blackGradient =LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFFE7E9BB).withOpacity(0.75),
      Color(0xFF403B4A).withOpacity(0.75)
    ],
  );

  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

}

enum PaymentMethods{
  CashOnDelivery,
  Card,
  UPI,
}



extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

extension Hex on Color {
  int fromHex(String hex) => 0xFF + int.parse(hex.substring(1));
}
