import 'package:flutter/material.dart';
import 'package:foomart/Components/BlockButton.dart';

import 'Components/CustomListTile.dart';
import 'Components/SearchAndLocationAppBar.dart';
import 'Constants.dart';

class OrderReview extends StatefulWidget {
  @override
  _OrderReviewState createState() => _OrderReviewState();
}

class _OrderReviewState extends State<OrderReview> {
  PaymentMethods _selectedPaymentMode = PaymentMethods.Card;
  bool _cardStatus = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedPaymentMode = PaymentMethods.Card;
    _cardStatus = false;
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return SafeArea(
        child: Scaffold(
      backgroundColor: Color(0xFFF9F9F9),
      body: Container(
        height: _size.height,
        width: _size.width,
        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        child: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SearchAndLocationAppBar(),
                SizedBox(height: 20),
                Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Delivery Address",
                          style: AppConstants.headingFont,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              flex: 5,
                              child: RichText(
                                text: TextSpan(
                                    text: "No. 30th, 11th main Road, \n",
                                    children: [
                                      TextSpan(
                                          text:
                                              "Vasanth Nagar, Miller's Road \n"),
                                      TextSpan(text: "Near Jain Hospital."),
                                    ],
                                    style: AppConstants.bodyFont
                                        .apply(
                                            fontWeightDelta: 2,
                                            fontSizeFactor: 0.8)
                                        .merge(TextStyle(height: 1.3))),
                              ),
                            ),
                            Flexible(
                                flex: 2,
                                child: TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    "Change",
                                    style: AppConstants.headingFont,
                                  ),
                                ))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(vertical: 4),
                          title: Text(
                            "Payment method",
                            style: AppConstants.headingFont
                                .apply(color: AppConstants.textGrey),
                          ),
                          trailing: TextButton(
                            style: TextButton.styleFrom(
                                padding: EdgeInsets.only(right: 0)),
                            onPressed: () {

                              showDialog(context: context, builder: (_){
                                return Container(
                                  padding: EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0)
                                  ),
                                  child: Scaffold(
                                    backgroundColor: Colors.transparent,
                                    body: Center(
                                      child: Form(
                                        child: Card(
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: SingleChildScrollView(
                                              child: Column(
                                                children: [
                                                  CustomListTile(
                                                    title: Text("Add Credit/Debit Card", style: AppConstants.bodyFont,),
                                                    trailing: GestureDetector(
                                                      onTap: ()=> Navigator.pop(_),
                                                      child: Icon(Icons.cancel_outlined),
                                                    ),
                                                  ),
                                                  Divider(),
                                                  SizedBox(height: 20,),
                                                  TextFormField(
                                                    decoration: InputDecoration(
                                                      border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(1000),
                                                      ),
                                                      fillColor: Colors.red,
                                                      hintText: "Card Number"
                                                    ),
                                                  ),
                                                  SizedBox(height: 20,),
                                                  TextFormField(
                                                    decoration: InputDecoration(
                                                        border: OutlineInputBorder(
                                                          borderRadius: BorderRadius.circular(1000),
                                                        ),
                                                        fillColor: Color(0xfff2f2f2),
                                                        hintText: "Card Number"
                                                    ),
                                                  ),
                                                  SizedBox(height: 20,),
                                                  TextFormField(
                                                    decoration: InputDecoration(
                                                        border: OutlineInputBorder(
                                                          borderRadius: BorderRadius.circular(1000),
                                                        ),
                                                        fillColor: Color(0xfff2f2f2),
                                                        hintText: "Card Number"
                                                    ),
                                                  ),
                                                  SizedBox(height: 20,),
                                                  TextFormField(
                                                    decoration: InputDecoration(
                                                        border: OutlineInputBorder(
                                                          borderRadius: BorderRadius.circular(1000),
                                                        ),
                                                        fillColor: Color(0xfff2f2f2),
                                                        hintText: "Card Number"
                                                    ),
                                                  ),
                                                  SizedBox(height: 20,),
                                                  TextFormField(
                                                    decoration: InputDecoration(
                                                        border: OutlineInputBorder(
                                                          borderRadius: BorderRadius.circular(1000),
                                                        ),
                                                        fillColor: Color(0xfff2f2f2),
                                                        hintText: "Card Number"
                                                    ),
                                                  ),
                                                  SizedBox(height: 20,),
                                                  SwitchListTile(
                                                    title: Text("Status"),
                                                    value: _cardStatus,
                                                    onChanged: (bool value) {
                                                      print(_cardStatus);
                                                      setState(() {
                                                        _cardStatus = value;
                                                      });
                                                    },
                                                  ),
                                                  SizedBox(height: 20,),
                                                  BlockButton(
                                                    btnLabel: "Save",
                                                    isRounded: true,
                                                  )

                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              });

                            },
                            child: Text("+ Add Cards"),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFF6F6F6),
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(color: AppConstants.appGrey)),
                          child: ListTile(
                            title: Text("Cash On Delivery"),
                            trailing: Icon(
                              _selectedPaymentMode ==
                                      PaymentMethods.CashOnDelivery
                                  ? Icons.brightness_1_rounded
                                  : Icons.brightness_1_outlined,
                              color: AppConstants.appGreen,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFF6F6F6),
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(color: AppConstants.appGrey)),
                          child: ListTile(
                            leading: Image.network(
                              "https://assets.stickpng.com/images/58482363cef1014c0b5e49c1.png",
                              width: 50,
                            ),
                            title: Text("**** **** **** 2345"),
                            trailing: Icon(
                              _selectedPaymentMode == PaymentMethods.Card
                                  ? Icons.brightness_1_rounded
                                  : Icons.brightness_1_outlined,
                              color: AppConstants.appGreen,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFF6F6F6),
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(color: AppConstants.appGrey)),
                          child: ListTile(
                            leading: Image.network(
                                "https://www.searchpng.com/wp-content/uploads/2019/02/Google-Pay-Logo-Icon-PNG.png"),
                            title: Text("UPI"),
                            trailing: Icon(
                              _selectedPaymentMode == PaymentMethods.UPI
                                  ? Icons.brightness_1_rounded
                                  : Icons.brightness_1_outlined,
                              color: AppConstants.appGreen,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("Sub Total",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 68",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("Delivery Cost",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 2",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("Discount",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 8",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("GST",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 2",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        Divider(),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(vertical: 8),
                          title: Text("Total",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 70",
                            style: AppConstants.headingFont,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:16.0, vertical: 10.0 ),
                  child: BlockButton(btnLabel: "Place Order"),
                )
              ]),
        ),
      ),
      bottomNavigationBar: Container(
        color: Color(0xFFf6f6f6),
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            height: 60,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: _size.width/2.5 - 16*2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.home_filled),
                        ),
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.bookmark),
                        ),
                      ],
                    ),
                  ),
                ),

                Positioned(
                  top: -20,
                  left: _size.width/2 - 45,
                  child: Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(1000),color: AppConstants.appGreen,),
                    child: GestureDetector(
                      onTap: ()=>{},
                      child: Icon(Icons.shopping_cart_rounded, color: Colors.white,),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: _size.width/2.5 - 16*2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.notifications),
                        ),
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.account_circle_rounded),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
    ),
    ));
  }
}
