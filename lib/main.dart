import 'package:flutter/material.dart';
import 'package:foomart/Components/LocationCheck.dart';
import 'package:foomart/ProductPage.dart';
import 'package:foomart/dashboard.dart';
import 'package:foomart/login.dart';
import 'package:foomart/Constants.dart';
import 'package:foomart/order_review.dart';
import 'package:foomart/register.dart';

import 'checkout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: AppConstants.createMaterialColor(AppConstants.appGreen),
        backgroundColor: Colors.white,
      ),
      home: OrderReview(),
    );
  }
}
