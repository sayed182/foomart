import 'package:flutter/material.dart';
import 'package:foomart/Components/ResturantCardVertical.dart';
import 'package:foomart/Components/ResturantCardWithCounter.dart';
import 'package:foomart/Constants.dart';

import 'Components/SearchAndLocationAppBar.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF9F9F9),
          body: Container(
            height: _size.height,
            width: _size.width,
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SearchAndLocationAppBar(),
                  SizedBox(
                    height: 20,
                  ),
                  Placeholder(
                    fallbackHeight: _size.height / 2 * _ratio,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Flexible(child: ListView(
                    children: [
                      Text(
                        "Starter Veg",
                        style: AppConstants.bodyFont
                            .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                      ),
                      SizedBox(height: 10.0,),
                      RestaurantCardWithCounter(),
                      RestaurantCardWithCounter(),
                      SizedBox(height: 10.0,),
                      Text(
                        "Starter Non Veg",
                        style: AppConstants.bodyFont
                            .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                      ),
                      SizedBox(height: 10.0,),
                      RestaurantCardWithCounter(),
                      RestaurantCardWithCounter(),
                      RestaurantCardWithCounter(),
                    ],
                  )),

                ]),
          ),
          bottomNavigationBar: Container(
            color: Color(0xFFf6f6f6),
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            height: 60,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: _size.width/2.5 - 16*2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.home_filled),
                        ),
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.bookmark),
                        ),
                      ],
                    ),
                  ),
                ),

                Positioned(
                  top: -20,
                  left: _size.width/2 - 45,
                  child: Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(1000),color: AppConstants.appGreen,),
                    child: GestureDetector(
                      onTap: ()=>{},
                      child: Icon(Icons.shopping_cart_rounded, color: Colors.white,),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: _size.width/2.5 - 16*2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.notifications),
                        ),
                        GestureDetector(
                          onTap: ()=>{},
                          child: Icon(Icons.account_circle_rounded),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
