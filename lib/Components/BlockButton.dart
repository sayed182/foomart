import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foomart/Constants.dart';

class BlockButton extends StatefulWidget {
  final String btnLabel;
  final Function btnFunction;
  final bool isRounded;
  const BlockButton({Key key,@required this.btnLabel, this.btnFunction, this.isRounded = false}) : super(key: key);

  @override
  _BlockButtonState createState() => _BlockButtonState();
}

class _BlockButtonState extends State<BlockButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        HapticFeedback.mediumImpact();
        widget.btnFunction();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: AppConstants.appGreen,
          borderRadius: widget.isRounded?BorderRadius.circular(1000.0):BorderRadius.circular(5.0)
        ),
        padding: EdgeInsets.symmetric(vertical: 22.0),
        child: Text(widget.btnLabel, style: AppConstants.bodyFont.apply(color: Colors.white), textAlign: TextAlign.center,),
      ),
    );
  }
}
