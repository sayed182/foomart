import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import '../Constants.dart';
import 'Pill.dart';

class RestaurantCardVertical extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return Container(
      margin: EdgeInsets.only(bottom: 7.0),
      width: _size.width,
      constraints: BoxConstraints(maxHeight: 160.0, minHeight: 120.0),
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        shadowColor: Colors.black.withOpacity(0.22),
        child: Row(
          children: [
            Flexible(
              flex: 1,
              child: Placeholder(
                color: Colors.blueGrey,
              ),
            ),
            Flexible(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Spacer(),
                        SizedBox(
                          width: 10,
                        ),
                        Pill(
                          label: Text(
                            "OPEN",
                            style: AppConstants.bodyFont.apply(
                                fontWeightDelta: 2,
                                fontSizeFactor: 0.6,
                                color: AppConstants.appGreen),
                          ),
                        ),
                        Pill(
                          icon: Icon(Icons.star, color: Colors.yellow,),
                          label: Text(
                            "4.5",
                            style: AppConstants.bodyFont.apply(
                                fontWeightDelta: 2,
                                fontSizeFactor: 0.6,
                                color: Color(0xff3E3F68)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Imperial Restaurant",
                      style: AppConstants.bodyFont
                          .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                    ),
                    Text(
                      "Millers Road",
                      style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7),
                    ),
                    Row(
                      children: [

                        Pill(
                          label: Text(
                            "Italian",
                            style: AppConstants.bodyFont
                                .apply(fontSizeFactor: 0.6, color: Colors.white),
                          ),
                          gradient: AppConstants.orangeGradient
                        ),
                        Pill(
                            label: Text(
                              "12 Km",
                              style: AppConstants.bodyFont
                                  .apply(fontSizeFactor: 0.6, color: Colors.white),
                            ),
                            gradient: AppConstants.greenGradient
                        ),
                        Pill(
                            label: Text(
                              "20 mins",
                              style: AppConstants.bodyFont
                                  .apply(fontSizeFactor: 0.6, color: Colors.white),
                            ),
                            gradient: AppConstants.blackGradient
                        ),

                      ],
                    ),
                    Row(
                      children: [
                        Spacer(),
                        Pill(
                          label: Text(
                            "Min Order \u20B9 200",
                            style: AppConstants.bodyFont
                                .apply(fontSizeFactor: 0.7, color: AppConstants.appGreen),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
