import 'package:flutter/material.dart';

import '../Constants.dart';

class Pill extends StatelessWidget {
  final Text label;
  final Icon icon;
  final Gradient gradient;

  const Pill({Key key, this.label, this.icon, this.gradient}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5.0),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: Colors.white,
          boxShadow: gradient != null?null:[
            BoxShadow(
                color: Colors.black.withAlpha(29),
                offset: Offset(0.0, 3.0),
                blurRadius: 8,)
          ],
        gradient: gradient??null,
      ),

      child: Row(
        children: [
          icon??Container(),
          label
        ],
      ),
    );
  }
}
