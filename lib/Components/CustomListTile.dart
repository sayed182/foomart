import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  final Widget leading;
  final Widget title;
  final Widget trailing;
  final EdgeInsets contentPadding;
  final BoxDecoration decoration;

  const CustomListTile({Key key, this.leading, this.title, this.trailing, this.contentPadding, this.decoration}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Widget _leading = Container();
    if(leading != null){

    }

    return Container(
      padding: contentPadding??EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      decoration: decoration??null,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            flex:3,
            child: title??Container(),
          ),
          Flexible(
            flex: 1,
            child: trailing??Container(),
          ),
        ]
      ),
    );
  }
}
