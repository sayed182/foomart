import 'package:flutter/material.dart';
import 'package:foomart/Constants.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchAndLocationAppBar extends StatefulWidget {
  @override
  _SearchAndLocationAppBarState createState() => _SearchAndLocationAppBarState();
}

class _SearchAndLocationAppBarState extends State<SearchAndLocationAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 3, vertical: 4),
      decoration: BoxDecoration(
        color: AppConstants.appGreen.withOpacity(0.35),
        borderRadius: BorderRadius.circular(5.0)
      ),
      child: Row(
        children: [
          Icon(Icons.location_pin, color: AppConstants.appGreen,),
          SizedBox(width: 10.0,),
          Text("MG Road", style: GoogleFonts.josefinSans(fontSize: 16, color: AppConstants.appGreen, fontWeight: FontWeight.w600,),),
          Spacer(),
          IconButton(icon: Icon(Icons.search, color: AppConstants.appGreen,), onPressed: (){})
        ],
      )
    );
  }
}
