import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import '../Constants.dart';
import 'Pill.dart';

class RestaurantCardWithCounter extends StatefulWidget {
  @override
  _RestaurantCardWithCounterState createState() => _RestaurantCardWithCounterState();
}

class _RestaurantCardWithCounterState extends State<RestaurantCardWithCounter> {
  int _itemCount = 1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _itemCount = 1;
  }
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return Container(
      margin: EdgeInsets.only(bottom: 7.0),
      width: _size.width,
      constraints: BoxConstraints(maxHeight: 140.0, minHeight: 120.0),
      child: Card(
        elevation: 10,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        shadowColor: Colors.black.withOpacity(0.22),
        child: Row(
          children: [
            Flexible(
              flex: 1,
              child: Placeholder(
                color: Colors.blueGrey,
              ),
            ),
            Flexible(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Tandoori Chicken",
                      style: AppConstants.bodyFont
                          .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                    ),
                    Row(
                      children: [
                        Pill(
                            label: Text(
                              "Full",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.6, color: Colors.white),
                            ),
                            gradient: LinearGradient(colors: [
                              AppConstants.appGreen,
                              AppConstants.appGreen
                            ])),
                        Pill(
                            label: Text(
                              "Half",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.6, color: Colors.white),
                            ),
                            gradient: LinearGradient(colors: [
                              AppConstants.appGrey,
                              AppConstants.appGrey,
                            ])),
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Text("Qty : "),
                        InkWell(
                          onTap: () {
                            if(_itemCount >0){
                              setState(() {
                                _itemCount--;
                              });
                            }
                          },
                          child: Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: AppConstants.appGreen,
                                borderRadius: BorderRadius.circular(5.0),
                                boxShadow: [BoxShadow(
                                  color: AppConstants.appGrey,
                                  offset: Offset(0.0,0.3),
                                  blurRadius: 6
                                )],
                              ),
                              child: Icon(
                                Icons.remove,
                                color: Colors.white,
                              )),
                        ),
                        Container(
                          height: 17.0 + 16,
                          width: 24.0 + 16,
                          margin: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: AppConstants.appGreen,width: 2.0),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child:Center(child: Text("${_itemCount}"))
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              _itemCount++;
                            });
                          },
                          child: Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: AppConstants.appGreen,
                                borderRadius: BorderRadius.circular(5.0),
                                boxShadow: [BoxShadow(
                                    color: AppConstants.appGrey,
                                    offset: Offset(0.0,0.3),
                                    blurRadius: 6
                                )],
                              ),
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                              )),
                        ),
                        Spacer(),
                        Pill(
                          label: Text(
                            "\u20B9 200",
                            style: AppConstants.bodyFont.apply(
                                fontSizeFactor: 0.7,
                                color: AppConstants.appGreen),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
